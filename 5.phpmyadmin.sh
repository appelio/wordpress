#!/bin/bash

# Install phpMyAdmin
yum install php-mbstring -y
systemctl restart httpd
systemctl restart php-fpm
wget -P /var/www/html https://www.phpmyadmin.net/downloads/phpMyAdmin-latest-all-languages.tar.gz
mkdir -p /var/www/html/phpMyAdmin && tar -xvzf /var/www/html/phpMyAdmin-latest-all-languages.tar.gz -C /var/www/html/phpMyAdmin --strip-components 1
rm /var/www/html/phpMyAdmin-latest-all-languages.tar.gz
