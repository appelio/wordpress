#!/bin/bash

# Configure and secure MariaDB
systemctl start mariadb
systemctl enable mariadb

# Make sure that NOBODY can access the server without a password
mysql -e "UPDATE mysql.user SET Password=PASSWORD('changeme') WHERE User='root';"
# Kill the anonymous users
mysql -e "DELETE FROM mysql.user WHERE User='';"
# Disallow remote login for root
mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
# Kill off the demo database
mysql -e "DROP DATABASE IF EXISTS test;"
# Make our changes take effect
mysql -e "FLUSH PRIVILEGES"
# Any subsequent tries to run queries this way will get access denied
